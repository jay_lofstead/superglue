#!/bin/bash

# FALCON

mpirun --oversubscribe -n 4 -H falcon2 lmp_falcon < in.cracktiny &> sim.out &
mpirun --oversubscribe -n 2 -H falcon3 select dump.custom.fp atoms 1 lmpselect.fp lmpsel vx vy vz &> sel.out &
mpirun --oversubscribe -n 2 -H falcon4 magnitude lmpselect.fp lmpsel velos.fp velocities &> mag.out &
mpirun --oversubscribe -n 2 -H falcon6 histogram velos.fp 16 velocities &> hist.out &

wait
