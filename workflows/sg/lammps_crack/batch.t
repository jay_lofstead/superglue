#!/bin/bash
#PBS -A CSC218
#PBS -N lammps-wf
#PBS -j oe
#PBS -V
#PBS -l walltime=00:10:00
#PBS -l nodes=64

#export CMTransportVerbose=1
#export CMTraceFile=1
#export CMSelfFormats=1
#export CMTransport="enet"
cd /lustre/atlas/scratch/acham/csc218/superglue/workflows/sg/lammps_crack

#aprun -n 65536 ./3D_Grid -g 4096x4096x4096 -l 64x128x128 &> grid.out &
#aprun -n 8192 $GEN/install/bin/histogram 3Ddump.fp 128 pressure density temperature &> histo.out & 
#aprun -n 2048 ./3D_Grid -g 4096x2048x1024 -l 256x128x128 &> grid.out &
#aprun -n 512 $GEN/histogram 3Ddump.fp 128 pressure density temperature &> histo.out &
#aprun -n 16 ./3D_Grid -g 256x256x256 -l 64x128x128 &> grid.out &
#aprun -n 160 $GEN/install/bin/select dump.custom.fp.fp 128 pressure &> histo.out &

#aprun -n 64 histogram \
#      velos.fp 16 velocities &> hist.out &
#aprun -n 256 magnitude \
#      lmpselect.fp lmpsel velos.fp velocities &> mag.out &
#aprun -n 256 select \
#      dump.custom.fp atoms 1 lmpselect.fp lmpsel vx vy vz &> sel.out &
aprun -n 1024 lmp_titan < in.cracksm &> lmp.out &

## LAMMPS + all-in-one analysis component launcher ##
#aprun -n 4 lmp_titan < in.cracktiny &> sim.out &
#aprun -n 4 lammps_analysis 16 &> analysis.out &

wait
