# intial run or restart run
irun 0
# number of time steps
mstep 20
# radial
mpsi 1800
# poloidal
mthetamax 12800
# toroidal
mzetamax 1
# number of pe in each radial
npe_radiald 1
# major radius
r0 1864
# nbound value
nbound 4
# collision parameters tauii > 0 collision < 0 no collision
tauii -1.0
# the rest of the parameters
# can be modified in setup.c
