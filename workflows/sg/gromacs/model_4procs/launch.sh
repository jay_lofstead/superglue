#!/bin/bash

mpirun -np 4 mdrun_mpi -s model.tpr -v -nsteps 10000 &> sim.out &
mpirun -n 4 magnitude arrays.fp positions magnitudes distances_to_zero &> magnitude.out &
mpirun -n 4 histogram magnitudes 16 distances_to_zero &> hist.out &

wait
