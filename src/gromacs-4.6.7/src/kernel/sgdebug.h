#ifndef SGDEBUG_H
#define SGDEBUG_H

#define sglog(...)                                              \
    fprintf(stderr, "[sglue (%s:%d)]: ", __FILE__, __LINE__);     \
    fprintf(stderr, __VA_ARGS__);                               \
    fflush(stderr);

#endif
