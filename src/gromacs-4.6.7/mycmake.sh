#!/bin/bash

cmake -DCMAKE_INSTALL_PREFIX:PATH=/autofs/nccs-svm1_home1/acham/proj/gromacs_install \
      -DGMX_BUILD_OWN_FFTW=ON \
      -DGMX_MPI=ON \
      -DBUILD_SHARED_LIBS=OFF \
      ..

#      -DREGRESSIONTEST_DOWNLOAD=ON \
