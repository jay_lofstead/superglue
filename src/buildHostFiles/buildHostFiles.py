#!/usr/bin/python

import sys

if len(sys.argv) < 3:
  print "not enough args"
  sys.exit()

# Variables
nodestr = ""
nodes = []
procnums = []
numnodes = 0
numprocs = 0
procsPerNode = int(sys.argv[2])
procsleftinnode = procsPerNode
currentprocs = 0
procsinnodeLocation = 0
currentNodeLocation = 0

# Open and read file
fname = sys.argv[1]
f = open(fname)
l = f.readline()

# Check value of first item read
lmod = l.rstrip('\n')
if not lmod:
  print "node file empty"
  sys.exit()
nodes.append(lmod)
nodestr += lmod

# Read rest of file
while 1:
  l = f.readline()
  if not l:
    break
  lmod = l.rstrip('\n')
  if (lmod not in nodes):
    nodes.append(lmod)
    nodestr += "," 
    nodestr += lmod

# build host files
if len(sys.argv) > 3:
  # debug
  # print "len sys.argv is " + `len(sys.argv)`
  for i in range(3, len(sys.argv)):
    procnums.append(int(sys.argv[i]))
  # procnums list full
  #debug
  #for z in range(0, len(procnums)):
    # print `procnums[z]` + " procs in component"
  prev_slots = -1
  for idx in range(0,len(procnums)):
    nodesfilledhere = 0
    thisComponentNodeList = []
    currentprocsleft = procnums[idx]

    while (1):
      if (currentprocsleft == (procsPerNode - procsinnodeLocation)):
        thisComponentNodeList.append(nodes[currentNodeLocation])
        nodesfilledhere += 1
        currentNodeLocation += 1
        procsinnodeLocation = 0
        slots = currentprocsleft
        break
      elif (currentprocsleft < (procsPerNode - procsinnodeLocation)):
        thisComponentNodeList.append(nodes[currentNodeLocation])
        procsinnodeLocation += currentprocsleft
        nodesfilledhere += 1
        slots = currentprocsleft
        break
      else: # more procs left than can fit in this node
        thisComponentNodeList.append(nodes[currentNodeLocation])
        currentprocsleft -= (procsPerNode - procsinnodeLocation)
        procsinnodeLocation = 0
        currentNodeLocation += 1
        nodesfilledhere += 1
        continue
    hostfilename = 'hosts'
    hostfilename += `idx + 1`
    outf = open(hostfilename, 'a')
    for x in range(0, nodesfilledhere):
      outf.write(thisComponentNodeList[x])
      if (x == 0 and prev_slots > 0):
        if (nodesfilledhere > 1):
          outf.write(" slots=" + str(procsPerNode - prev_slots) + "\n")
      elif (x < (nodesfilledhere - 1)):
        outf.write(" slots=" + str(procsPerNode) + "\n")
      else:
        prev_slots = slots
        outf.write(" slots=" + str(slots) + "\n")
    outf.close()

# Now we have alist of the node names with 
# no repetition

# output
print "host files created"
# print nodests
